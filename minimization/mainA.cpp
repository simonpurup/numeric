#include <iostream>
#include <stdio.h>
#include <armadillo>
#include "../tools/newton/newton.h"

using namespace std;
using namespace arma;

double rosenbrock_f(vec p);
vec rosenbrock_gradient(vec p);
mat rosenbrock_hessian(vec p);
double himmelbau_f(vec p);
vec himmelbau_gradient(vec p);
mat himmelbau_hessian(vec p);
double three_hump_camel(vec p);
vec three_hump_camel_gradient(vec p);
mat three_hump_camel_hessian(vec p);


int main(){
    vec x(2);
    x(0) = 5; x(1) = 10;
    cout << "Finding stationary point of the Rosenbrock valley function:" << endl;
    cout << "       f(x,y) = (1-x)\u00B2+100(y-x\u00B2)\u00B2" << endl<<endl;
    cout << "Using Newton's method with simple back-tracking line search" << endl;
    cout << "--------------------------------------------------------------------" << endl;
    double fx = rosenbrock_f(x);
    printf("Starting point is:  (%g,%g)\n",x(0),x(1));
    printf("With f(%g,%g) =      %g\n\n",x(0),x(1),fx);
    int rosen_steps = newton_minimization(&rosenbrock_f,&rosenbrock_gradient,&rosenbrock_hessian,x,1e-4);
    fx = rosenbrock_f(x);
    vec dfx = rosenbrock_gradient(x);
    cout << "Solution found in " << rosen_steps << " steps" << endl;
    printf("Solution is:        (%g,%g)\n",x(0),x(1));
    printf("With f = %g  and  \u2207f = (%g,%g)\n",fx,dfx(0),dfx(1));

    x(0) = 5; x(1) = 10; //Reset the starting point
    cout << endl << endl << "====================================================================" << endl;
    cout << "Finding stationary point of the Himmelbau function:" << endl;
    cout << "       f(x,y) = (x\u00B2+y-11)\u00B2+(x+y\u00B2-7)\u00B2" << endl<<endl;
    cout << "Using Newton's method with simple back-tracking line search" << endl;
    cout << "--------------------------------------------------------------------" << endl;
    fx = himmelbau_f(x);
    printf("Starting point is:  (%g,%g)\n",x(0),x(1));
    printf("With f(%g,%g) =      %g\n\n",x(0),x(1),fx);
    int himmel_steps = newton_minimization(&himmelbau_f,&himmelbau_gradient,&himmelbau_hessian,x,1e-4);
    fx = himmelbau_f(x);
    dfx = himmelbau_gradient(x);
    cout << "Solution found in " << himmel_steps << "  steps" << endl;
    printf("Solution is:        (%g,%g)\n",x(0),x(1));
    printf("With f = %g  and  \u2207f = (%g,%g)\n",fx,dfx(0),dfx(1));

    x(0) = 5; x(1) = 10; //Reset the starting point
    cout << endl << endl << "====================================================================" << endl;
    cout << "Finding stationary point of the Three hump camel function :" << endl;
    cout << "       f(x,y) = x²-1.05x⁴+x⁶/6+x*y+y²" << endl<<endl;
    cout << "Using Newton's method with simple back-tracking line search" << endl;
    cout << "--------------------------------------------------------------------" << endl;
    fx = himmelbau_f(x);
    printf("Starting point is:  (%g,%g)\n",x(0),x(1));
    printf("With f(%g,%g) =      %g\n\n",x(0),x(1),fx);
    int camel_steps = newton_minimization(&three_hump_camel,&three_hump_camel_gradient,&three_hump_camel_hessian,x,1e-6);
    fx = three_hump_camel(x);
    dfx = three_hump_camel_gradient(x);
    cout << "Solution found in " << camel_steps << "  steps" << endl;
    printf("Solution is:        (%g,%g)\n",x(0),x(1));
    printf("With f = %g  and  \u2207f = (%g,%g)\n",fx,dfx(0),dfx(1));
}

double rosenbrock_f(vec p){
    double x = p(0), y = p(1);
    double f1 = 1-x, f2 = y-x*x;
    return f1*f1+100*f2*f2;
}

vec rosenbrock_gradient(vec p){
    vec grad(2);
    double x = p(0), y = p(1);
    grad(0) = 2.0*(200.0*x*x*x-200.0*x*y+x-1);
    grad(1) = 200.0*(y-x*x);
    return grad;
}

mat rosenbrock_hessian(vec p){
    mat hessian(2,2);
    double x = p(0), y = p(1);
    hessian(0,0) = 1200*x*x-400*y+2;
    hessian(1,1) = 200.0;
    hessian(0,1) = hessian(1,0) = -400.0*x;
    return hessian;
}

double himmelbau_f(vec p){
    double x = p(0), y = p(1);
    double f1 = x*x+y-11, f2 = x+y*y-7;
    return f1*f1+f2*f2;
}

vec himmelbau_gradient(vec p){
    vec grad(2);
    double x = p(0), y = p(1);
    grad(0) = 2.0*(2.0*x*(x*x+y-11)+x+y*y-7);
    grad(1) = 2.0*(x*x+2.0*y*(x+y*y-7)+y-11);
    return grad;
}

mat himmelbau_hessian(vec p){
    mat hessian(2,2);
    double x = p(0), y = p(1);
    hessian(0,0) = 4.0*(3.0*x*x+y-11)+2;
    hessian(1,1) = 4.0*(x+3.0*y*y-7)+2;
    hessian(0,1) = hessian(1,0) = 4.0*y+4.0*y;
    return hessian;
}

double three_hump_camel(vec p){
    double x = p(0), y = p(1);
    return 2.0*pow(x,2)-1.05*pow(x,4)+pow(x,6)/6+x*y+y*y;
}

vec three_hump_camel_gradient(vec p){
    vec grad(2);
    double x = p(0), y = p(1);
    grad(0) = x*x*x*x*x-4.2*x*x*x+4.0*x+y;
    grad(1) = x+2.0*y;
    return grad;
}

mat three_hump_camel_hessian(vec p){
    mat hessian(2,2);
    double x = p(0), y = p(1);
    hessian(0,0) = 5.0*pow(x,4)-12.6*x*x+4;
    hessian(1,1) = 2.0;
    hessian(0,1) = 1.0;
    return hessian;
}