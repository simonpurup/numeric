#include <iostream>
#include <stdio.h>
#include <armadillo>
#include <math.h>
#include "../tools/newton/newton.h"

using namespace std;
using namespace arma;

double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int n;

double F(vec p);
double f(double A, double T, double B, double t);

int main(){
    n = sizeof(t)/ sizeof(t[0]);

    for(int i=0; i<n; i++){
        printf("%g %g %g\n",t[i],y[i],e[i]);
    }
    cout << endl << endl;

    vec x(3);
    x(0) = 10; x(1) = 5; x(2) = 1;
    quasi_newton_broyden(&F,x,1e-4,1e-6);
    double A = x(0), T = x(1), B = x(2);
    double ti = 0.1;
    while(ti<10){
        printf("%g %g\n",ti,f(A,T,B,ti));
        ti += 0.1;
    }
    return 0;
}

double f(double A, double T, double B, double t){
    return A*exp(-t/T)+B;
}

double F(vec p){
    double result = 0;
    double A = p(0), T = p(1), B = p(2);
    double ft, delta;
    for(int i=0;i<n;i++){
        ft = f(A,T,B,t[i]);
        delta = ft-y[i];
        result += delta*delta/(e[i]*e[i]);
    }
    return result;
}
