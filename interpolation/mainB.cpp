#include <iostream>
#include "qspline.h"
#include <stdlib.h>

int main(){
    int n = 20;
    vector<double> x(n), y(n);
    for (unsigned int i = 0; i < x.size(); i++) {
        x[i] = 2*i*M_PI/(n-1);
        y[i] = sin(x[i]);
        cout << x[i] << "   " << y[i] << endl;
    }
    QSpline spline(x,y);
    cout << endl << endl; // New data set (for gnuplot)
    double z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.eval(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    cout << endl << endl; // New data set (for gnuplot)
    z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.deriv(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    cout << endl << endl; // New data set (for gnuplot)
    z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.integ(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    return 0;
}

/* Constructor for the QSpline class. Initializes the data set and calculates
 * coefficients c and b
 */
QSpline::QSpline(vector<double> x, vector<double> y) {
    QSpline::x = x;
    QSpline::y = y;
    n = x.size();
    vector<double> c_temp(n-1), b_temp(n-1);
    c = c_temp; b = b_temp;
    //Calculate c and b from the given data set (see note on interpolation)
    vector<double> dx(n), p(n), c_down(n-1), c_up(n-1);
    int i;
    for(i=0;i<n-1;i++){ //Intermediate parameters
        dx[i] = x[i+1]-x[i];
        p[i] = (y[i+1]-y[i])/dx[i];
    }
    c_up[0] = 0; c_down[n-2] = 0;
    for(i=0; i<n-2; i++){ // Calculate c by up recursion
        c_up[i+1] = 1.0/dx[i+1]*(p[i+1]-p[i]-c_up[i]*dx[i]);
    }
    for(i=n-3; i>=0; i--){ // Calculate c by down recursion
        c_down[i] = 1.0/dx[i]*(p[i+1]-p[i]-c_down[i+1]*dx[i+1]);
    }
    for(i=0; i<n-1; i++){ // Take the average as c and calculate b
        c[i] = (c_up[i]+c_down[i])/2.0;
        b[i] = p[i]-c[i]*dx[i];
    }
}

/* Destructor: Delete the allocated variables (delete for memory allocated with new and
 * free() for memory allocated with malloc).
 */
QSpline::~QSpline(){ }

double QSpline::eval(double z) {
    int i = binarySearch(z);
    return y[i]+b[i]*(z-x[i])+c[i]*pow((z-x[i]),2);
}

double QSpline::deriv(double z) {
    int i = binarySearch(z);
    return b[i]+2*c[i]*(z-x[i]);
}

double QSpline::integ(double z) {
    int i = binarySearch(z);
    double integ = 0, dx;
    int k = 0;
    if(i>0) {
        for (k=0; k < i-1; k++) {
            dx = x[k+1]-x[k];
            integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx);
        }
        dx = z-x[k];
        integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx);
    } else {
        dx = z-x[k];
        integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx);
    }
    return integ;
}

int QSpline::binarySearch(double z) {
    int i = 0, j = n-1;
    // Binary search for z in x
    while(j-i>1){
        int mid = (i+j)/2;
        if(z>x[mid])
            i = mid;
        else
            j = mid;
    }
    return i;
}

