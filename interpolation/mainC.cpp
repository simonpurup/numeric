#include <iostream>
#include "cspline.h"
#include <stdlib.h>

int main(){
    int n = 20;
    vector<double> x(n), y(n);
    for (unsigned int i = 0; i < x.size(); i++) {
        x[i] = 2*i*M_PI/(n-1);
        y[i] = sin(x[i]);
        cout << x[i] << "   " << y[i] << endl;
    }
    CSpline spline(x,y);
    cout << endl << endl; // New data set (for gnuplot)
    double z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.eval(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    cout << endl << endl; // New data set (for gnuplot)
    z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.deriv(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    cout << endl << endl; // New data set (for gnuplot)
    z = 0.01;
    while (z < x[n-1]) {
        double ans = spline.integ(z);
        cout << z << "   " << ans << endl;
        z += 0.01;
    }
    return 0;
}

/* Constructor for the QSpline class. Initializes the data set and calculates
 * coefficients c and b
 */
CSpline::CSpline(vector<double> x, vector<double> y) {
    CSpline::x = x;
    CSpline::y = y;
    n = x.size();
    vector<double> c_temp(n-1), b_temp(n), d_temp(n-1);
    c = c_temp; b = b_temp; d = d_temp;
    //Calculate c, b and d from the given data set (see note on interpolation)
    vector<double> dx(n-1), p(n-1), D(n), Q(n-1), B(n);
    for(int i=0;i<n-1;i++){ //Intermediate parameters
        dx[i] = x[i+1]-x[i];
        p[i] = (y[i+1]-y[i])/dx[i];
    }
    D[0] = 2; D[n-1] = 2;
    Q[0] = 1;
    B[0] = 3*p[0]; B[n-1] = 3*p[n-2];
    for(int i=0;i<n-2;i++){ //Generate the tridiagonal system
        D[i+1] = 2.0*dx[i]/dx[i+1]+2.0;
        Q[i+1] = dx[i]/dx[i+1];
        B[i+1] = 3.0*(p[i]+p[i+1]*dx[i]/dx[i+1]);
    }
    for(int i=1;i<n;i++){ //Gauss elimination
        D[i] -= Q[i-1]/D[i-1];
        B[i] -= B[i-1]/D[i-1];
    }
    b[n-1] = B[n-1]/D[n-1];
    for(int i=n-2;i>=0;i--){ //Back substitution
        b[i] = (B[i]-Q[i]*b[i+1])/D[i];
    }
    for(int i=0;i<n-1;i++){
        c[i] = (-2.0*b[i]-b[i+1]+3.0*p[i])/dx[i];
        d[i] = (b[i]+b[i+1]-2.0*p[i])/dx[i]/dx[i];
    }
}

/* Destructor: Delete the allocated variables (delete for memory allocated with new and
 * free() for memory allocated with malloc).
 */
CSpline::~CSpline(){ }

double CSpline::eval(double z) {
    int i = binarySearch(z);
    double dx = z-x[i];
    return y[i]+b[i]*(dx)+c[i]*pow(dx,2)+d[i]*pow(dx,3);
}

double CSpline::deriv(double z) {
    int i = binarySearch(z);
    return b[i]+2.0*c[i]*(z-x[i])+3.0*d[i]*pow((z-x[i]),2);
}

double CSpline::integ(double z) {
    int i = binarySearch(z);
    double integ = 0, dx;
    int k = 0;
    if(i>0) {
        for (k=0; k < i-1; k++) {
            dx = x[k+1]-x[k];
            integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx+1.0/4.0*d[k]*dx*dx*dx);
        }
        dx = z-x[k];
        integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx+1.0/4.0*d[k]*dx*dx*dx);
    } else {
        dx = z-x[k];
        integ += dx*(y[k]+0.5*b[k]*dx+1.0/3.0*c[k]*dx*dx+1.0/4.0*d[k]*dx*dx*dx);
    }
    return integ;
}

int CSpline::binarySearch(double z) {
    int i = 0, j = n-1;
    // Binary search for z in x
    while(j-i>1){
        int mid = (i+j)/2;
        if(z>x[mid])
            i = mid;
        else
            j = mid;
    }
    return i;
}



