#include <iostream>
#include "qspline.h"


using namespace std;
typedef unsigned long uLong;

double linterp(double z, vector<double> x, vector<double> y);
double linterp_integ(double z, vector<double> x, vector<double> y);

int main() {
    int n = 20;
    vector<double> x(n), y(n); // Create a data set of sine function from 0 to 2pi
    for (uLong i = 0; i < x.size(); i++) {
        x[i] = 2*i*M_PI/(n-1);
        y[i] = sin(x[i]);
        cout << x[i] << "   " << y[i] << endl;
    }
    cout << endl << endl; // New data set (for gnuplot)
    double i = 0.01;
    while (i < x[n-1]) {
        double ans = linterp(i, x, y);
        cout << i << "   " << ans << endl;
        i += 0.01;
    }
    cout << endl << endl; // New data set (for gnuplot)
    i = 0.01;
    while (i < x[n-1]) {
        double ans = linterp_integ(i, x, y);
        cout << i << "   " << ans << endl;
        i += 0.01;
    }
    return 0;
}

/* Function for doing linear interpolation given
 */
double linterp(double z, vector<double> x, vector<double> y) {
    uLong n = x.size();
    assert(n>1 && z>x[0] && z<x[n-1]);
    uLong i = 0, j = n-1;
    // Binary search for z in x
    while(j-i>1){
        uLong mid = (i+j)/2;
        if(z>x[mid])
            i = mid;
        else
            j = mid;
    }
    // Return the value obtained from linear interpolation.
    return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linterp_integ(double z, vector<double> x, vector<double> y){
    uLong n = x.size();
    assert(n>1 && z>x[0] && z<x[n-1]);
    uLong i = 0, j = n-1;
    // Binary search for z in x
    // TODO: should probably be moved to a separate function to avoid code duplication.
    while(j-i>1){
        uLong mid = (i+j)/2;
        if(z>x[mid])
            i = mid;
        else
            j = mid;
    }
    double integ = 0, p = 0;
    uLong k = 0;
    if(i>0) {
        for (k=0; k < i-1; k++) {
            p = (y[k+1]-y[k])/(x[k+1]-x[k]);
            integ += 0.5*(x[k]-x[k+1])*(p*(x[k]-x[k+1])-2.0*y[k]);
        }
        p = (y[k+1]-y[k])/(x[k+1]-x[k]);
        integ += 0.5*(x[k]-z)*(p*(x[k]-z)-2.0*y[k]);
    } else {
        p = (y[k+1]-y[k])/(x[k+1]-x[k]);
        integ += 0.5*(x[k]-z)*(p*(x[k]-z)-2.0*y[k]);
    }
    return integ;
}



