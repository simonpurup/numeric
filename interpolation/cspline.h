#include <vector>
#include <math.h>
#include <assert.h>

using namespace std;

class CSpline {
private:
    vector<double> x,y,b,c,d;
    int n;
    int binarySearch(double z);
public:
    CSpline(vector<double> x,vector<double> y);
    ~CSpline();
    double eval(double z);
    double deriv(double z);
    double integ(double z);
};
