#include <vector>
#include <math.h>
#include <assert.h>

using namespace std;

class QSpline {
private:
    vector<double> x,y,b,c;
    int n;
    int binarySearch(double z);
public:
    QSpline(vector<double> x,vector<double> y);
    ~QSpline();
    double eval(double z);
    double deriv(double z);
    double integ(double z);
};
