#include <armadillo>

using namespace arma;

#ifndef NUMERIC_ODE_STEPPERS_H
void rkstep23(double t, double h, vec& y,vec (*f)(double,vec&), vec& yh, vec& err);
#define NUMERIC_ODE_STEPPERS_H

#endif //NUMERIC_ODE_STEPPERS_H
