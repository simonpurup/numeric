#include <armadillo>
#include "ode_steppers.h"

using namespace arma;

void rkstep23(double t, double h, vec& y,vec (*f)(double,vec&), vec& yh, vec& err){
    vec yt;
    vec k0 = f(t,y);
    yt = y+k0*h/2;
    vec k1 = f(t+h/2,yt);
    yt = y+3.0/4*h*k1;
    vec k2 = f(t+3.0/4*h,yt);

    yh = y+h*(2.0/9*k0+1.0/3*k1+4.0/9*k2);
    vec k3 = f(t+h,yh);
    yt = y+h*(7.0/24*k0+1.0/4*k1+1.0/3*k2+1.0/8*k3);
    err = yh-yt;
}

