#include <iostream>
#include <stdio.h>
#include <iostream>
#include "ode_steppers.h"

using namespace std;
using namespace arma;
typedef void (*Stepper)(double,double,vec&,vec (*)(double,vec&),vec&,vec&);

int driver(rowvec& t_list, mat& y_list, double b, double& h, double acc, double eps,
           Stepper stepper, vec (*f)(double,vec&), int max);
vec recycle_brine_tank_cascade(double t,vec& y);

int main(){
    int max = 1e3;
    rowvec t_list(max);
    mat y_list(3,max);

    t_list(0) = 0; //Starting points
    y_list(0,0) = 1; y_list(1,0) = 1; y_list(2,0) = 1;
    double b = 30, h = 1e-2, acc = 1e-3, eps = 1e-3;
    int k = driver(t_list,y_list,b,h,acc,eps,&rkstep23,&recycle_brine_tank_cascade,max);

    cout << "Part A - solving the recycled brine tank cascade: " << endl;
    cout << "   y\u2081' = -1/6y\u2081+1/6y\u2083" << endl;
    cout << "   y\u2082' = 1/6y\u2081-1/3y\u2082" << endl;
    cout << "   y\u2083' = 1/3y\u2082-1/6y\u2083" << endl;
    cout << "The sum of y should remain constant and as t\u2192\u221E the " <<
            "ratio between y\u2081, y\u2082, and y\u2083 should be 2:1:2" << endl;
    cout << "--------------------------------------------------------------" << endl;
    cout << "ODE with: b=30, acc=10⁻³, eps=10⁻³\nt\u2080=0, y\u2080=(1,1,1)" << endl;
    printf("Final values:   t=%g   y=(%g,%g,%g)\n",t_list(k),y_list(0,k),y_list(1,k),y_list(2,k));

    cout << endl << endl;
    double s;
    for(int i=0;i<=k;i++){
        s = accu(y_list.col(i))/3;
        printf("%g %g %g %g %g\n",t_list(i),y_list(0,i),y_list(1,i),y_list(2,i),s);
    }
}

/** System can be found in http://www.math.utah.edu/~gustafso/2250systems-de.pdf
 * As t goes to infinity the brine should distribute itself in the ratio 2:1:2
 */
vec recycle_brine_tank_cascade(double t,vec& y){
    vec yh(3);
    yh(0) = -y(0)/6+y(2)/6;
    yh(1) = y(0)/6-y(1)/3;
    yh(2) = y(1)/3-y(2)/6;
    return yh;
}


