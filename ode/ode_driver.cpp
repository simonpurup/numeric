#include <armadillo>
#include <math.h>
#include "ode_steppers.h"

using namespace arma;
typedef void (*Stepper)(double,double,vec&,vec (*)(double,vec&),vec&,vec&);

/* Driver for ODE solver. The t_list provided holds at first the starting point, t. The matrix y_list
 * holds at first the initial values of y in the first column (:,0). The length of t_list and y_list must
 * be the same as the parameter max, this is to avoid reallocation of memory required if the size is to be
 * increased. *
 * TODO: Implementing a linked list, as intermediate storage, will solve this problem, however implementing an efficient
 *       linked list is a big task in itself.
 * The user also provides the stepper to be used, as well as the initial step size, and the relative
 * and absolute tolerances. The endpoint is decided by b.
 */
int driver(rowvec& t_list, mat& y_list, double b, double& h, double acc, double eps,
            Stepper stepper, vec (*f)(double,vec&), int max){
    int k = 0;
    double t = t_list(0);
    vec yh(3), dy(3), y = y_list.col(0);
    double err, normy, tol, a = t;
    while(t<b){
        if(t+h>b)
            h = b-t;
        stepper(t,h,y,f,yh,dy);
        err = norm(dy);
        normy = norm(yh);
        tol = (normy*eps+acc)*sqrt(h/(b-a));
        if(err<tol){
            k++;
            if(k > max-1)
                return k;
            t_list[k] = t+h;
            y_list.col(k) = yh;
        }
        if(err!=0)
            h *= pow(tol/err,0.25)*0.95;
        else
            h *= 2;
        t = t_list(k);
        y = y_list.col(k);
    }
    return k;
}

