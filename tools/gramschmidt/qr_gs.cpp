#include <assert.h>
#include "qr_gs.h"

using namespace arma;

void qr_gs_decomp(mat& A, mat& R){
    // QR-decomposition of the give matrix A. A is replaced with Q and R is filled
    for(uword i=0; i<A.n_cols; i++){
        R(i,i) = sqrt(as_scalar(A.col(i).t()*A.col(i)));
        A.col(i) = A.col(i)/R(i,i);
        for(uword j=i+1; j<A.n_cols; j++){
            R(i,j) = as_scalar(A.col(i).t()*A.col(j));
            A.col(j) = A.col(j)-A.col(i)*R(i,j);
        }
    }
}

void qr_gs_solve(mat& Q, mat& R, vec& b, vec& c){
    //Given Q, R (from qr_gs_decomp) and b, solves the linear equation by back-substitution.
    //The solution is stored in the vector c. Used for non-square matrices, where b is not the
    //same length as the answer, c.
    b = Q.t()*b;
    for(int i = R.n_cols-1; i >= 0; i--){
        double s = 0;
        for(unsigned int k = i+1; k<R.n_cols; k++)
            s+=R(i,k)*c(k);
        c(i) = (b(i)-s)/R(i,i);
    }
}

void qr_gs_solve(mat& Q, mat& R, vec& b){
    //Given Q, R (from qr_gs_decomp) and b, solves the linear equation by back-substitution.
    //The vector b is replaced with the solution x.
    b = Q.t()*b;
    for(int i = R.n_cols-1; i >= 0; i--){
        double s = 0;
        for(unsigned int k = i+1; k<R.n_cols; k++)
            s+=R(i,k)*b(k);
        b(i) = (b(i)-s)/R(i,i);
    }
}

void qr_gs_inverse(mat& Q, mat& R, mat& B){
    //Calculates the inverse of matrix A and stores it in B
    assert(Q.n_cols == Q.n_rows);
    int n = Q.n_cols;
    vec e = vec(n);
    for(int i = 0; i < n; i++){
        e(i) = 1;
        qr_gs_solve(Q,R,e);
        B.col(i) = e;
        e.zeros();
    }
}


