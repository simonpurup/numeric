#include <armadillo>
using namespace arma;

#ifndef NUMERIC_QR_GS_H
void qr_gs_decomp(mat& A,mat& R);
void qr_gs_solve(mat& Q, mat& R, vec& b,vec& c);
void qr_gs_solve(mat& Q, mat& R, vec& b);
void qr_gs_inverse(mat& Q, mat& R, mat& B);
#define NUMERIC_QR_GS_H
#endif //NUMERIC_QR_GS_H
