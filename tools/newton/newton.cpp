#include <armadillo>
#include "../gramschmidt/qr_gs.h"
#include "newton.h"

using namespace arma;

int newton_root(vec (*f)(vec), vec& x, double tolerance, double dx){
    int steps = 0;
    int n = x.n_elem;
    mat J(n,n), R(n,n);
    vec df(n), y(n), fy(n), Dx(n);
    vec fx = f(x);
    double norm_fx = norm(fx);
    while(norm_fx >=tolerance){ //While the function value is not zero within the given tolerance, do:
        steps++;
        for(int j = 0; j<n;j++){ //Numerically evaluate the Jacobian matrix.
            x(j) += dx;
            df = f(x)-fx;
            for(int i = 0; i<n;i++){
                J(i,j) = df(i)/dx;
            }
            x(j) -= dx;
        }
        qr_gs_decomp(J,R);  //Solve the equation system J*Dx=-f(x):
        qr_gs_solve(J,R,fx,Dx); //As a pointer (fx) must be passed, we instead solve J*Dx'=f(x)
        Dx = (-1)*Dx;           //and just set Dx = -Dx'

        double lambda = 1;
        while(true){ //Simple backtracking line search
            y = x+Dx*lambda;
            fy = f(y);
            if(norm(fy)<(1-lambda/2)*norm_fx || lambda<1.0/64)
                break;
            lambda /= 2;
        }
        x = y;
        fx = fy;
        norm_fx = norm(fx);
    }
    return steps;
}

int newton_root(vec (*f)(vec), mat (*jacobian)(vec), vec& x, double tolerance){
    int steps = 0;
    int n = x.n_elem;
    mat J, R(n,n);
    vec df(n), y(n), fy(n), Dx(n);
    vec fx = f(x);
    double norm_fx = norm(fx);
    while(norm_fx >=tolerance){ //While the function value is not zero within the given tolerance, do:
        steps++;
        J = jacobian(x); //Use the provided Jacobian matrix
        qr_gs_decomp(J,R);  //Solve the equation system J*Dx=-f(x):
        qr_gs_solve(J,R,fx,Dx); //As a pointer (fx) must be passed, we instead solve J*Dx'=f(x)
        Dx = (-1)*Dx;           //and just set Dx = -Dx'

        double lambda = 1;
        while(true){ //Simple backtracking line search
            y = x+Dx*lambda;
            fy = f(y);
            if(norm(fy)<(1-lambda/2)*norm_fx || lambda<1.0/64)
                break;
            lambda /= 2;
        }
        x = y;
        fx = fy;
        norm_fx = norm(fx);
    }
    return steps;
}

/* Finds a local minimum for the given function, f, provided the gradient vector and
 * Hessian (second derivative) matrix. The gradient and Hessian must be provided by functions
 * that calculates them for a given position, x, i.e. calculated analytically.
 */
int newton_minimization(double (*f)(vec), vec (*gradient)(vec), mat (*hessian)(vec),vec& x, double tolerance){
    int steps = 0;
    int alpha = 1e-2; //Parameter for the Armijo condition, can be chosen as low as 1e-4
    int n = x.n_elem;
    double fx = f(x), fy;
    mat H, R(n,n);
    vec Dx(n), y(n);
    vec Dfx = gradient(x);

    while(norm(Dfx) >=tolerance){ //While the norm of the gradient is not zero within the given tolerance, do:
        steps++;
        H = hessian(x);
        qr_gs_decomp(H,R);      //Solve the equation system H*Dx=-Df(x):
        qr_gs_solve(H,R,Dfx,Dx);    //As a pointer (fx) must be passed, we instead solve H*Dx'=-Df(x)
        Dx = (-1)*Dx;               //and just set Dx = -Dx'

        double lambda = 1;
        while(true){ //Simple backtracking line search
            y = x+Dx*lambda;
            fy = f(y);
            if(fy<(fx+alpha*lambda*as_scalar(Dx.t()*Dfx)) || lambda<1.0/64) //Armijo condition
                break;
            lambda /= 2;
        }
        x = y;
        fx = fy;
        Dfx = gradient(x);
    }
    return steps;
}

/* Finds a local minimum for the given function, f, provided the gradient, using quasi-Newton method with
 * Broyden's update to estimate the inverse of the Hessian matrix.
 * This is one of two overloaded functions, this one numerically estimates the gradient.
 */
int quasi_newton_broyden(double (*f)(vec), vec& x, double tolerance, double dx){
    int steps = 0, n = x.n_elem;
    int alpha = 1e-2; //Parameter for the Armijo condition, can be chosen as low as 1e-4
    double fz, denom, fx = f(x);
    vec s(n), y(n), z(n), Dx(n), Dfx(n), Dfx_z(n);

    mat H_inv(n,n); //Inverse Hessian matrix
    H_inv.eye(); //Set the initial inverse Hessian matrix to be the unit matrix
    while(true){
        steps++;
        for(int i = 0;i<n;i++){ //Numerically estimate the gradient
            x(i) += dx;
            Dfx(i) = (f(x)-fx)/dx;
            x(i) -= dx;
        }

        Dx = -H_inv*Dfx;
        s = Dx;
        while(true){ //Simple backtracking line search
            z = x+s;
            fz = f(z);
            if(fz<(fx+alpha*as_scalar(s.t()*Dfx))) //Armijo condition
                break;
            if(norm(s) < 1e-6) { //If the step becomes too small, reset.
                H_inv.eye();
                break;
            }
            s /= 2;
        }
        for(int i = 0;i<n;i++){ //Numerically estimate the gradient
            z(i) += dx;
            Dfx_z(i) = (f(z)-fz)/dx;
            z(i) -= dx;
        }
        y = Dfx_z-Dfx;
        denom = as_scalar(y.t()*H_inv*s);
        if(denom<1e-9) //If the update diverges, reset the Hessian matrix.
            H_inv.eye();
        else
            H_inv += (s-H_inv*y)*s.t()*H_inv/denom;
        x = z;
        fx = f(x);
        Dfx = Dfx_z;
        if(norm(Dfx) < tolerance)
            break;
    }
    return steps;
}

/* Finds a local minimum for the given function, f, provided the gradient, using quasi-Newton method with
 * Broyden's update to estimate the inverse of the Hessian matrix.
 * This is one of two overloaded functions, this one requires the user to provide the gradient.
 */
int quasi_newton_broyden(double (*f)(vec), vec (*gradient)(vec), vec& x, double tolerance){
    int steps = 0, n = x.n_elem;
    int alpha = 1e-2; //Parameter for the Armijo condition, can be chosen as low as 1e-4
    double fy, denom, fx = f(x);
    vec s(n), y(n), z(n), Dx(n), Dfx = gradient(x);

    mat H_inv(n,n); //Inverse Hessian matrix
    H_inv.eye(); //Set the initial inverse Hessian matrix to be the unit matrix
    while(norm(Dfx) >= tolerance){
        steps++;
        Dx = -H_inv*Dfx;
        s = Dx;
        while(true){ //Simple backtracking line search
            z = x+s;
            fy = f(z);
            if(fy<(fx+alpha*as_scalar(s.t()*Dfx))) //Armijo condition
                break;
            if(norm(s) < 1e-6) { //If the step becomes too small, reset.
                H_inv.eye();
                break;
            }
            s /= 2;
        }
        y = gradient(z)-Dfx;
        denom = as_scalar(y.t()*H_inv*s);
        if(denom<1e-9) //If the update diverges, reset the Hessian matrix.
            H_inv.eye();
        else
            H_inv += (s-H_inv*y)*s.t()*H_inv/denom;
        x = z;
        fx = f(x);
        Dfx = gradient(x);
    }
    return steps;
}