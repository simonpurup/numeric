#include <armadillo>

using namespace arma;

#ifndef NUMERIC_NEWTON_ROOT_H
int newton_root(vec (*f)(vec), vec& x, double tolerance, double dx);
int newton_root(vec (*f)(vec), mat (*jacobian)(vec), vec& x, double tolerance);
int newton_minimization(double (*f)(vec), vec (*gradient)(vec), mat (*hessian)(vec),vec& x, double tolerance);
int quasi_newton_broyden(double (*f)(vec), vec (*gradient)(vec), vec& x, double tolerance);
int quasi_newton_broyden(double (*f)(vec), vec& x, double tolerance, double dx);
#define NUMERIC_NEWTON_ROOT_H

#endif //NUMERIC_NEWTON_ROOT_H
