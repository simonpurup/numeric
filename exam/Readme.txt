Simon Purup Eskildsen   Studiekort nr.: 201403913
I have chosen to do project 8 - Jacobi diagonalization with classic sweeps and indexing

I have implemented the classical jacobi diagonalization algorithm, zeroing the largest off-diagonal element with each
rotation. The largest off-diagonal element is found using a vector that holds the indices of the largest off-diagonal
element in each row. This vector is updated after each rotation, only considering the elements changed so that the
update only require O(n) operations instead of O(n²).

To compare the effectiveness of the classical method I also compare the number of rotations and the CPU time required
to diagonalize the same matrix with the classic method versus the cyclic sweep method (with the same precision).