#include <armadillo>
#include <cmath>  //fabs
#include "jacobi.h"

using namespace arma;

/* Overloaded function that uses default threshold equal to 1e-6.
 */
int jacobi_classical(mat& A, vec& e, mat& V){
    return jacobi_classical(A,e,V,1e-6);
}

/*Implementation of the classical jacobi diagonalization with indexing.
 * Here the user provides the threshold (after diagonalization the largest off-diagonal element will be smaller than
 * the provided threshold).
 */
int jacobi_classical(mat& A, vec& e, mat& V, double threshold) {
    int rotations = 0;
    bool converged = false;
    int n = A.n_cols;
    e = A.diag(); // Set e to be the diagonal of A
    V.eye(); // Set V to be identity matrix

    vec miv(n-1); //Vector that holds the indices of the largest off-diagonal element in each row (miv = max index vector)
    for (int i = 0; i < n-1; ++i) { //Fill in the index vector (row n-1 has no off diagonal elements)
        int max_j = -1;
        for (int j = i+1; j < n; ++j) {
            if(max_j == -1 || fabs(A(i,max_j)) < fabs(A(i,j)))
                max_j = j; //Find the index of the largest element in row i.
        }
        miv(i) = max_j;
    }

    int p, q;
    do {
        converged = true;
        rotations++;

        int max_i = -1;
        for(int i = 0; i<(int)miv.n_elem; i++){ //Find the indices of the largest off diagonal element
            if(max_i == -1 || fabs(A(max_i,miv(max_i))) < fabs(A(i,miv(i))))
                max_i = i;
        }
        p = max_i;
        q = miv(p);

        double A_pp = e(p), A_qq = e(q), A_pq = A(p, q); //And zero it
        double phi = 0.5 * atan2(2 * A_pq, (A_qq - A_pp)); //Calculate the angle for jacobi rotation
        double c = cos(phi), s = sin(phi);
        double A_pp_new = c*c*A_pp-2.0*s*c*A_pq+s*s*A_qq;
        double A_qq_new = s*s*A_pp+2.0*s*c*A_pq+c*c*A_qq;
        if (fabs(A(p,q)) > threshold) { //If the maximum off-diagonal element is larger than some threshold:
            converged = false;  //then the algorithm has not converged
            e(p) = A_pp_new;
            e(q) = A_qq_new;
            A(p,q) = 0.0;
            for(int i = 0; i<p; i++){
                double A_ip = A(i,p), A_iq = A(i,q);
                A(i,p) = c*A_ip-s*A_iq; //
                A(i,q) = c*A_iq+s*A_ip;
                //Update the indices vector, if any of the new elements are now the largest element in row i
                if(fabs(A(i,p)) > fabs(A(i,miv(i))))
                    miv(i) = p;
                if(fabs(A(i,q)) > fabs(A(i,miv(i))))
                    miv(i) = q;
            }
            for(int i=p+1; i<q; i++){
                double A_pi = A(p,i), A_iq = A(i,q);
                A(p,i) = c*A_pi-s*A_iq;
                A(i,q) = c*A_iq+s*A_pi;
                //Update the indices vector
                if(fabs(A(p,i)) > fabs(A(p,miv(p))))
                    miv(p) = i;
                if(fabs(A(i,q)) > fabs(A(i,miv(i))))
                    miv(i) = q;
            }
            for(int i=q+1; i<n; i++){
                double A_pi = A(p,i), A_qi = A(q,i);
                A(p,i) = c*A_pi-s*A_qi;
                A(q,i) = c*A_qi+s*A_pi;
                //Update the indices vector
                if(fabs(A(p,i)) > fabs(A(p,miv(p))))
                    miv(p) = i;
                if(fabs(A(q,i)) > fabs(A(q,miv(q))))
                    miv(q) = i;
            }
            for(int i = 0; i<n; i++){
                double V_ip = V(i,p), V_iq = V(i,q);
                V(i,p) = c*V_ip-s*V_iq;
                V(i,q) = c*V_iq+s*V_ip;
            }
        }
    } while (!converged);
    return rotations;
}

int jacobi_sweep(mat& A, vec& e, mat& V) {
    int rotations = 0;
    bool changed = false;
    int n = A.n_cols;
    e = A.diag(); // Set e to be the diagonal of A
    V.eye(); // Set V to be identity matrix

    do {
        changed = false;
        for (int q = n - 1; q>0; q--) {
            for (int p = 0; p<q; p++) {
                rotations++;
                double A_pp = e(p), A_qq = e(q), A_pq = A(p, q);
                double phi = 0.5 * atan2(2 * A_pq, (A_qq - A_pp)); //Calculate the angle for jacobi rotation
                double c = cos(phi), s = sin(phi);
                double A_pp_new = c*c*A_pp-2.0*s*c*A_pq+s*s*A_qq;
                double A_qq_new = s*s*A_pp+2.0*s*c*A_pq+c*c*A_qq;
                if (A_pp_new != A_pp || A_qq_new != A_qq) {
                    changed = true;
                    e(p) = A_pp_new;
                    e(q) = A_qq_new;
                    A(p, q) = 0.0;
                    for(int i = 0; i<p; i++){
                        double A_ip = A(i,p), A_iq = A(i,q);
                        A(i,p) = c*A_ip-s*A_iq;
                        A(i,q) = c*A_iq+s*A_ip;
                    }
                    for(int i=p+1; i<q; i++){
                        double A_pi = A(p,i), A_iq = A(i,q);
                        A(p,i) = c*A_pi-s*A_iq;
                        A(i,q) = c*A_iq+s*A_pi;
                    }
                    for(int i=q+1; i<n; i++){
                        double A_pi = A(p,i), A_qi = A(q,i);
                        A(p,i) = c*A_pi-s*A_qi;
                        A(q,i) = c*A_qi+s*A_pi;
                    }
                    for(int i = 0; i<n; i++){
                        double V_ip = V(i,p), V_iq = V(i,q);
                        V(i,p) = c*V_ip-s*V_iq;
                        V(i,q) = c*V_iq+s*V_ip;
                    }
                }
            }
        }
    } while (changed);

    return rotations;
}