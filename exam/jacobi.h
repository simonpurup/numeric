#include <armadillo>

using namespace arma;

#ifndef NUMERIC_JACOBI_H
int jacobi_classical(mat& A, vec& e, mat& V);
int jacobi_classical(mat& A, vec& e, mat& V, double threshold);
int jacobi_sweep(mat& A, vec& e, mat& V);
#define NUMERIC_JACOBI_H

#endif //NUMERIC_JACOBI_H
