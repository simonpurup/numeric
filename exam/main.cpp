#include <iostream>
#include <armadillo>
#include <cmath>
#include <time.h>
#include "jacobi.h"

using namespace std;
using namespace arma;

void randSymMat(mat& A);

int main(){
    cout.precision(4); //Set the precision for the output stream
    cout.setf(ios::fixed);
    srand(time(NULL)); //Set seed for random number generator
    int size = 10;
    mat A(size,size);
    randSymMat(A);
    mat A_init = A;
    mat V(size,size);
    vec e(size);

    cout << "Jacobi diagonalization:" << endl;
    cout << "--------------------------------------------------" << endl;
    A.raw_print(cout,"Initial matrix A:"); //Print the initial matrix
    cout << endl << endl;
    cout << "Solved with classical jacobi diagonalization: " << endl;

    jacobi_classical(A,e,V); //Perform classical jacobi

    e.raw_print(cout,"Eigenvalues:");
    cout << endl << endl;
    mat VTAV = V.t()*A_init*V; // Calculate V transposed * A * V
    VTAV.raw_print(cout,"V^T*A*V:");
    cout << endl << endl;

    //----------------Comparison between cyclic sweep and classical----------------------
    size = 100; //Use larger matrix for comparison
    mat A1(size,size);
    randSymMat(A1);
    mat A1_init = A1;
    mat V1(size,size);
    vec e1(size);

    clock_t t_sweep; //Used to measure CPU time
    t_sweep = clock();
    int rotations_sweep = jacobi_sweep(A1,e1,V1); //First use sweep, to determine threshold for classical
    t_sweep = clock()-t_sweep;

    int max_j = -1, max_i = -1;
    for (int i = 0; i < size-1; ++i) { //Find the largest off-diagonal element
        for (int j = i+1; j < size; ++j) {
            if(max_i == -1 || fabs(A1(max_i,max_j)) < fabs(A1(i,j)))
                max_i = i;
            max_j = j;
        }
    }
    double threshold = fabs(A1(max_i,max_j)); //For a fair comparison, require same precision of both algorithms

    A1 = A1_init;
    V1.zeros(); e1.zeros(); //Reset matrices and vectors.

    clock_t t_classical;
    t_classical = clock();
    int rotations = jacobi_classical(A1,e1,V1,threshold); //Perform classical jacobi
    t_classical = clock()-t_classical;

    cout << "Comparison between sweep and classical, 100x100 matrix (same precision)" << endl;
    cout << "Number of rotations required: " << endl;
    cout << "    Classical: " << rotations << endl;
    cout << "    Sweep:     " << rotations_sweep << endl;
    cout << "Run-time: " << endl;
    cout << "    Classical: " << ((float)t_classical)/(CLOCKS_PER_SEC/1e3) << " ms" << endl;
    cout << "    Sweep:     " << ((float)t_sweep)/(CLOCKS_PER_SEC/1e3) << " ms" << endl;
    return 0;
}

/* Generates a random symmetric matrix from A. */
void randSymMat(mat& A){
    for(int i = 0; i<(int)A.n_cols; i++){
        for(int j = 0; j<=i; j++){
            double f = (double) rand()/RAND_MAX;
            double v = f*100; //random number in the interval [0,99]
            A(i,j) = v;
            A(j,i) = v;
        }
    }
}

