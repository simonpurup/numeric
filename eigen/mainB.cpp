#include <iostream>
#include <armadillo>
#include <cmath>
#include <time.h>
#include <iomanip>

using namespace std;
using namespace arma;

int jacobi_row_by_row(int n_rows, mat& A, vec& e, mat& V, bool highest_first);
void randSymMat(mat& A);

int main() {
    cout.precision(4); //Set the precision for the output stream
    cout.setf(ios::fixed);
    srand(time(NULL)); //Set seed for random number generator

    int size = 5;
    mat A(size, size);
    randSymMat(A); //Create random symmetric matrix.
    mat A_init = A;
    mat V(size, size);
    vec e(size);

    int rotations;
    cout << "Initial matrix A:" << endl << A << endl << endl;

    cout << "#####################################" << endl;
    cout << "#          Find lowest first        #" << endl;
    cout << "#####################################" << endl;
    for (int i = 1; i <= size; i++) {
        rotations = jacobi_row_by_row(i, A, e, V, false);
        cout << "Find " << i << " lowest eigenvalues" << endl;
        cout << "-----------------------------------" << endl;
        cout << "First " << i << " eigenvalues:" << endl;
        vec e_part = e(span(0,i-1));
        e_part.raw_print(cout);
        mat A_part = A(span(0,i-1),span::all);
        cout << endl << "First " << i << " rows in A" << endl;
        A_part.raw_print(cout);
        cout << endl << "Number of rotations required: " << rotations << endl;
        cout << endl << endl;

        A = A_init;
    }

    cout << "-----------------------------------" << endl;
    mat VTAV = V.t()*A*V;
    VTAV.raw_print(cout,"V^T*A*V:");
    cout << endl << endl;

    cout << "#####################################" << endl;
    cout << "#          Find highest first       #" << endl;
    cout << "#####################################" << endl;
    for (int i = 1; i <= size; i++) {
        rotations = jacobi_row_by_row(i, A, e, V, true);
        cout << "Find " << i << " lowest eigenvalues" << endl;
        cout << "-----------------------------------" << endl;
        cout << "First " << i << " eigenvalues:" << endl;
        vec e_part = e(span(0,i-1));
        e_part.raw_print(cout);
        mat A_part = A(span(0,i-1),span::all);
        cout << endl << "First " << i << " rows in A" << endl;
        A_part.raw_print(cout);
        cout << endl << "Number of rotations required: " << rotations << endl;
        cout << endl << endl;

        A = A_init;
    }
    cout << "-----------------------------------" << endl;
    VTAV = V.t()*A*V;
    VTAV.raw_print(cout,"V^T*A*V:");
    return 0;
}

/* Generates a random symmetric matrix from A. */
void randSymMat(mat& A){
    for(int i = 0; i<(int)A.n_cols; i++){
        for(int j = 0; j<=i; j++){
            double f = (double) rand()/RAND_MAX;
            double v = f*100; //random number in the interval [0,99]
            A(i,j) = v;
            A(j,i) = v;
        }
    }
}

int jacobi_row_by_row(int n_rows, mat& A, vec& e, mat& V, bool highest_first) {
    int rotations = 0;
    bool changed = false;
    int n = A.n_cols;
    e = A.diag(); // Set e to be the diagonal of A
    V.eye(); // Set V to be identity matrix
    for (int p = 0; p<n_rows; p++) { //Perform the row sweep for the number of rows specified
        do { //The sweep only runs over one row at a time
            changed = false;
            for (int q = p+1; q < n; q++) {
                rotations++;
                double A_pp = e(p), A_qq = e(q), A_pq = A(p, q);
                double phi = 0.5*atan2(2*A_pq, (A_qq - A_pp)); //Calculate the angle for jacobi rotation
                if(highest_first)
                     phi += 0.5*M_PI; // Changing the angle by 1/2pi makes it find highest first
                double c = cos(phi), s = sin(phi);
                double A_pp_new = c*c*A_pp-2.0*s*c*A_pq+s*s*A_qq;
                double A_qq_new = s*s*A_pp+2.0*s*c*A_pq+c*c*A_qq;
                if (A_pp_new != A_pp || A_qq_new != A_qq) {
                    changed = true;
                    e(p) = A_pp_new;
                    e(q) = A_qq_new;
                    A(p, q) = 0.0;
                    //The loop from i = 0 to i<p, changing A(i,q) and A(i,p) has been removed, as all elements used are
                    //zero, as diagonalizing is done row by row.
                    for(int i=p+1; i<q; i++){
                        double A_pi = A(p,i), A_iq = A(i,q);
                        A(p,i) = c*A_pi-s*A_iq;
                        A(i,q) = c*A_iq+s*A_pi;
                    }
                    for(int i=q+1; i<n; i++){
                        double A_pi = A(p,i), A_qi = A(q,i);
                        A(p,i) = c*A_pi-s*A_qi;
                        A(q,i) = c*A_qi+s*A_pi;
                    }
                    for(int i = 0; i<n; i++){
                        double V_ip = V(i,p), V_iq = V(i,q);
                        V(i,p) = c*V_ip-s*V_iq;
                        V(i,q) = c*V_iq+s*V_ip;
                    }
                }
            }
        } while (changed);
    }
    return rotations;
}