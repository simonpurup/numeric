#include <iostream>
#include <armadillo>
#include <cmath>
#include <time.h>

using namespace std;
using namespace arma;

int jacobi_sweep(mat& A, vec& e, mat& V);
void randSymMat(mat& A);

int main(){
    cout.precision(4); //Set the precision for the output stream
    cout.setf(ios::fixed);
    srand(time(NULL)); //Set seed for random number generator
    int size = 5;
    mat A(size,size);
    randSymMat(A);
    mat A_init = A;
    mat V(size,size);
    vec e(size);

    A.raw_print(cout,"Initial matrix A:"); //Print the initial matrix
    cout << endl << endl;

    int rotations = jacobi_sweep(A,e,V); //Perform jacobi with sweep

    e.raw_print(cout,"Eigenvalues:");
    cout << endl << endl;
    mat VTAV = V.t()*A_init*V; // Calculate V transposed * A * V
    VTAV.raw_print(cout,"V^T*A*V:");
    cout << endl << endl << "Number of rotations required: " << rotations;

    return 0;
}

/* Generates a random symmetric matrix from A. */
void randSymMat(mat& A){
    for(int i = 0; i<(int)A.n_cols; i++){
        for(int j = 0; j<=i; j++){
            double f = (double) rand()/RAND_MAX;
            double v = f*100; //random number in the interval [0,99]
            A(i,j) = v;
            A(j,i) = v;
        }
    }
}

int jacobi_sweep(mat& A, vec& e, mat& V) {
    int rotations = 0;
    bool changed = false;
    int n = A.n_cols;
    e = A.diag(); // Set e to be the diagonal of A
    V.eye(); // Set V to be identity matrix

    do {
        changed = false;
        for (int q = n - 1; q>0; q--) {
            for (int p = 0; p<q; p++) {
                rotations++;
                double A_pp = e(p), A_qq = e(q), A_pq = A(p, q);
                double phi = 0.5 * atan2(2 * A_pq, (A_qq - A_pp)); //Calculate the angle for jacobi rotation
                double c = cos(phi), s = sin(phi);
                double A_pp_new = c*c*A_pp-2.0*s*c*A_pq+s*s*A_qq;
                double A_qq_new = s*s*A_pp+2.0*s*c*A_pq+c*c*A_qq;
                if (A_pp_new != A_pp || A_qq_new != A_qq) {
                    changed = true;
                    e(p) = A_pp_new;
                    e(q) = A_qq_new;
                    A(p, q) = 0.0;
                    for(int i = 0; i<p; i++){
                        double A_ip = A(i,p), A_iq = A(i,q);
                        A(i,p) = c*A_ip-s*A_iq;
                        A(i,q) = c*A_iq+s*A_ip;
                    }
                    for(int i=p+1; i<q; i++){
                        double A_pi = A(p,i), A_iq = A(i,q);
                        A(p,i) = c*A_pi-s*A_iq;
                        A(i,q) = c*A_iq+s*A_pi;
                    }
                    for(int i=q+1; i<n; i++){
                        double A_pi = A(p,i), A_qi = A(q,i);
                        A(p,i) = c*A_pi-s*A_qi;
                        A(q,i) = c*A_qi+s*A_pi;
                    }
                    for(int i = 0; i<n; i++){
                        double V_ip = V(i,p), V_iq = V(i,q);
                        V(i,p) = c*V_ip-s*V_iq;
                        V(i,q) = c*V_iq+s*V_ip;
                    }
                }
            }
        }
    } while (changed);

    return rotations;
}

