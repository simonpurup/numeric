#include <iostream>
#include "../tools/gramschmidt/qr_gs.h"

using namespace std;
using namespace arma;

int main(){
    cout.precision(4); //Set the precision of the output stream
    cout.setf(ios::fixed);
    // Create the matrices A and R. A is filled with randoms number from a uniform distribution
    // of the numbers in the [0,1] interval, all entries in R is just 0.
    mat A(5,3,fill::randu);
    mat R(3,3);
    mat A_init = A;
    A.raw_print(cout,"Initial matrix A:"); //Print the initial matrix
    cout << endl << endl;

    qr_gs_decomp(A,R); //Perform decomposition

    R.raw_print(cout,"Matrix R:");
    cout << endl << endl;
    A.raw_print(cout,"Matrix Q:");
    cout << endl << endl;

    mat QTQ = A.t()*A; //Calculate Q transposed Q
    QTQ.raw_print(cout,"Q^TQ:");
    cout << endl << endl;

    mat A_re = A*R; //Recalculate A
    A_re.raw_print(cout,"Matrix A recalculated (Q*R):");
    cout << endl << endl;

    mat A_diff = A_init-A_re;
    A_diff.raw_print(cout,"A-Q*R:");
    return 0;
}




