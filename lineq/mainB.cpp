#include <iostream>
#include <armadillo>
#include <assert.h>
#include "../tools/gramschmidt/qr_gs.h"

using namespace std;
using namespace arma;

int main(){
    cout.precision(4); //Set the precision of the output stream
    cout.setf(ios::fixed);
    // Create the matrices A and R. A is filled with randoms number from a uniform distribution
    // of the numbers in the [0,1] interval, all entries in R is just 0.
    int size = 4;
    mat A(size,size,fill::randu);
    mat R(size,size);
    mat B = R;
    mat A_init = A;
    A.raw_print(cout,"Initial matrix A:" ); //Print the initial matrix
    cout << endl << endl;

    qr_gs_decomp(A,R); //Decompose
    qr_gs_inverse(A,R,B); //Calculate the inverse

    B.raw_print(cout,"Inverse of A:"); //Print the inverse matrix of A
    cout << endl << endl;

    mat Ax = B*A_init;
    Ax.raw_print(cout,"A^-1*A:");
    return 0;
}
