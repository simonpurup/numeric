#include <iostream>
#include <armadillo>
#include "../tools/gramschmidt/qr_gs.h"

using namespace std;
using namespace arma;

int main(){
    cout.precision(4); //Set the precision of the output stream
    cout.setf(ios::fixed);
    // Create the matrices A and R. A is filled with randoms number from a uniform distribution
    // of the numbers in the [0,1] interval, all entries in R is just 0.
    int size = 8;
    mat A(size,size,fill::randu);
    mat R(size,size);
    vec b(size,fill::randu);
    fmat A_init = conv_to<fmat>::from(A);
    A.raw_print(cout,"Initial matrix A:"); //Print the initial matrix
    cout << endl << endl;
    b.raw_print(cout,"Initial vector b:"); //Print the initial vector
    cout << endl << endl;

    qr_gs_decomp(A,R); //Perform decomposition
    qr_gs_solve(A,R,b); //Solve the linear equation system

    b.raw_print(cout,"Solved vector x:"); //vector b is now x, print it
    cout << endl << endl;

    mat Ax = A_init*b; //Recalculate b as A*x
    Ax.raw_print(cout,"A*x (should be equal to b):");
    return 0;
}