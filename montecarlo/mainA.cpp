#include <iostream>
#include <stdio.h>
#include <armadillo>
#include <math.h>

#define PI 3.14159265358979323846264338327950;

using namespace std;
using namespace arma;

void plainmc(double (*f)(vec), vec a, vec b, int N, double& I, double& sigma);
double f_sphere(vec R);
double f_1mcoscoscos(vec R);

int main(){
    vec a(3),b(3);
    a(0) = 0.0; a(1) = 0.0; a(2) = 0.0;
    b(0) = 1.0; b(1) = PI; b(2) = 2*PI; //(r,θ,φ)

    double I, sigma;
    int N = 1e6;
    plainmc(&f_sphere,a,b,N,I,sigma);
    cout << "Integrating over the volume of a sphere with radius 1 - should be 4/3PI = 4.1887902" << endl;
    cout << "For N = 10⁶:" << endl;
    printf("    Result: %g\n    Error:  %g",I,sigma);
    cout << endl << endl << endl;

    N = 1e6;
    b(0) = PI; b(1) = PI; b(2) = PI;
    plainmc(&f_1mcoscoscos,a,b,N,I,sigma);
    cout << "Integrating function in exercise - should be 1.3932039296857" << endl;
    cout << "For N = 10⁶:" << endl;
    printf("    Result: %g\n    Error:  %g",I,sigma);
    return 0;
}

double f_sphere(vec R){
    double r = R(0), theta = R(1);
    return r*r*sin(theta);
}

double f_1mcoscoscos(vec R){
    double pi = PI;
    return 1.0/(1-cos(R(0))*cos(R(1))*cos(R(2)))/(pi*pi*pi);
}
