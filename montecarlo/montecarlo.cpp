#include <armadillo>
#include <random>
#include <cmath>
#include <math.h>
#include <cfloat>

using namespace arma;
using namespace std;

/*After some research people advice using uniform_real_distribution over rand(), as the resolution is better, and
 *that the numbers generated are uniformly distributed.
 *As uniform_real_distribution is inclusive-exclusive on bounds, we have to use nextafter(1.0,DBL_MAX) as upper
 *bound in order to include 1.
 */
default_random_engine generator;
uniform_real_distribution<double> distribution(0.0,nextafter(1.0,DBL_MAX));

vec randomX(vec& a,vec& b){
    vec x(a.n_elem);
    for(int i = 0; i<(int)a.n_elem; i++) {
        double f = distribution(generator); //Uniformly distributed double in range [0,1]
        x(i) = a(i)+f*(b(i)-a(i));
    }
    return x;
}

/*Performs a plain Monte Carlo multi-dimensional integration of the function provided. The result and the error
 *are stored in the provided parameters I and sigma respectively.
 */
void plainmc(double (*f)(vec), vec a, vec b, int N, double& I, double& sigma){
    double volume = 1.0;
    for(int i=0;i<(int)a.n_elem;i++){ volume *= b(i)-a(i); }
    double sum = 0, sum_sq = 0;
    for(long int i=0;i<N;i++){
        double fx = f(randomX(a,b));
        sum += fx;
        sum_sq +=fx*fx;
    }
    double mean = sum/N;
    double S = sqrt(sum_sq/N-mean*mean)/sqrt(N);
    I = mean*volume;
    sigma = S*volume;
}

