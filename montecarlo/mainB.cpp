#include <iostream>
#include <stdio.h>
#include <armadillo>
#include <math.h>
#include "../tools/newton/newton.h"

#define PI 3.14159265358979323846264338327950;

using namespace std;
using namespace arma;

void plainmc(double (*f)(vec), vec a, vec b, int N, double& I, double& sigma);
double f_sphere(vec R);
double F(vec p);
double f_sqrt(double A, double B,double t);

double *y;
int *t;
int n;

int main(){
    vec a(3),b(3);
    a(0) = 0.0; a(1) = 0.0; a(2) = 0.0;
    b(0) = 1.0; b(1) = PI; b(2) = 2*PI; //(r,θ,φ)

    double I, sigma;
    int Ns[] = {10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000};
    n = sizeof(Ns)/ sizeof(Ns[0]);
    double sigmas[n];
    for(int i = 0; i<n; i++){
        plainmc(&f_sphere,a,b,Ns[i],I,sigma);
        sigmas[i] = sigma;
        printf("%i %g\n",Ns[i],sigma);
    }
    cout << endl << endl;
    y = sigmas; t = Ns;
    vec x(3);
    x(0) = 1; x(1) = 1;
    quasi_newton_broyden(&F,x,1e-4,1e-6);
    for(int i = 0; i<n; i++){
        printf("%i %g\n",Ns[i],f_sqrt(x(0),x(1),Ns[i]));
    }
}

double f_sphere(vec R){
    double r = R(0), theta = R(1);
    return r*r*sin(theta);
}

double f_sqrt(double A, double B,double t){
    return A/(sqrt(t))+B;
}

double F(vec p){
    double result = 0;
    double A = p(0), B = p(1);
    double ft, delta;
    for(int i=0;i<n;i++){
        ft = f_sqrt(A,B,t[i]);
        delta = ft-y[i];
        result += delta*delta;
    }
    return result;
}