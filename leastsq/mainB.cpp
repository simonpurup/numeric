#include <iostream>
#include <stdio.h>
#include <armadillo>
#include <math.h>
#include "leastsqfit.h"

using namespace std;
using namespace arma;

rowvec functions(double x);

int main(){
    double data_x[] = {0.1, 0.145, 0.211, 0.307, 0.447, 0.649, 0.944, 1.372,  1.995, 2.9};
    double data_y[] = {12.644, 9.235, 7.377, 6.46, 5.555, 5.896, 5.673, 6.964, 8.896, 11.355};
    double data_dy[] = {0.858, 0.359, 0.505, 0.403, 0.683, 0.605, 0.856, 0.351, 1.083, 1.002};
    int n = (int) sizeof(data_x)/ sizeof(data_x[0]);
    vec x(n), y(n), dy(n);
    mat S(3,3);

    for(int i = 0;i<n;i++){
        x(i) = data_x[i];
        y(i) = data_y[i];
        dy(i) = data_dy[i];
        printf("%g %g %g\n",data_x[i],data_y[i],data_dy[i]);
    }
    cout << endl << endl;

    vec c = leastsqfit(3,&functions,x,y,dy,S);
    vec dc = S.diag();
    for(int i = 0; i<(int)dc.n_elem; i++){
        dc(i) = sqrt(dc(i));
    }
    vec c_plus = c+dc, c_minus = c-dc;

    double fx = 0.05, fy, fy_plus, fy_minus;
    double inc = 0.05;
    while(fx<=3){
        fy = c(0)*1/fx+c(1)+c(2)*fx;
        fy_plus = c_plus(0)*1/fx+c_plus(1)+c_plus(2)*fx;
        fy_minus = c_minus(0)*1/fx+c_minus(1)+c_minus(2)*fx;
        printf("%g %g %g %g\n",fx,fy,fy_plus,fy_minus);
        fx+=inc;
    }
    return 0;
}

rowvec functions(double x){
    rowvec r(3);
    r(0) = 1.0/x;
    r(1) = 1.0;
    r(2) = x;
    return r;
}

