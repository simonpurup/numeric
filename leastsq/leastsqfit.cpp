#include <armadillo>
#include "../tools/gramschmidt/qr_gs.h"
#include "leastsqfit.h"

using namespace arma;

/* Function performing an ordinary least-squares fit on the given data (x,y,dy), where dy is the error on y.
 * The function, F, that is to be fitted to the data is provided by a function that takes a double and returns a
 * 1xm matrix of resulting values from the m functions that F is a linear combination of.
 * One of two overloaded functions, this function does not calculate the covariance matrix.
 */
vec leastsqfit(int m,rowvec (*funct)(double),vec& x,vec& y,vec& dy){
    int n = (int)x.n_elem;
    mat A(n,m), R(m,m);
    vec b(n), c(m);

    for(int i=0;i<n;i++){
        b(i) = y(i)/dy(i);
        A.row(i) = (*funct)(x(i))/dy(i);
    }
    qr_gs_decomp(A,R); //QR-decomposes A, Q is stored in A and R is stored in the provided matrix
    qr_gs_solve(A,R,b,c); //Solves the linear equation system, and stores the solution (coefficients) in b.
    return c; //Return the coefficients.
}

/* Function performing an ordinary least-squares fit on the given data (x,y,dy), where dy is the error on y.
 * The function, F, that is to be fitted to the data is provided by a function that takes a double and returns a
 * 1xm matrix of resulting values from the m functions that F is a linear combination of.
 * One of two overloaded functions, this function does calculate the covariance matrix and stores it in the provided
 * matrix, covarianceMat.
 */
vec leastsqfit(int m,rowvec (*funct)(double),vec& x,vec& y,vec& dy,mat& covarianceMat){
    int n = x.n_elem;
    mat A(n,m), R(m,m), inverseR(m,m), I(m,m);
    vec b(n), c(m);

    for(int i=0;i<n;i++){
        b(i) = y(i)/dy(i);
        A.row(i) = (*funct)(x(i))/dy(i);
    }
    qr_gs_decomp(A,R);
    qr_gs_solve(A,R,b,c);

    I.eye();
    qr_gs_inverse(I,R,inverseR);
    covarianceMat = inverseR*inverseR.t();
    return c;
}

