#include <armadillo>
using namespace arma;

#ifndef NUMERIC_LEASTSQFIT_H
vec leastsqfit(int m,rowvec (*funct)(double),vec& x,vec& y,vec& dy);
vec leastsqfit(int m,rowvec (*funct)(double),vec& x,vec& y,vec& dy,mat& covarianceMat);
#define NUMERIC_LEASTSQFIT_H

#endif //NUMERIC_LEASTSQFIT_H
