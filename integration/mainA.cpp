#include <iostream>
#include <stdio.h>
#include <math.h>
#include "integrator.h"

using namespace std;

int calls;
double f_sqrt(double x);
double f_sqrt_inv(double x);
double f_ln_sqrt_inv(double x);
double f_pi(double x);

void print_result(double Q);

int main(){
    cout << "Using absolute and relative tolerance equal to 10⁻⁶"<< endl << endl;
    cout << "Integrating sqrt(x) from x=0 to x=1 - should be 2/3:" << endl;
    Integrator integrator(&f_sqrt);
    print_result(integrator.integ(0,1,1e-6,1e-6));
    cout << "Integrating 1/sqrt(x) from x=0 to x=1 - should be 2:" << endl;
    print_result(integrator.integ(&f_sqrt_inv,0,1,1e-6,1e-6));
    cout << "Integrating ln(x)/sqrt(x) from x=0 to x=1 - should be -4:" << endl;
    print_result(integrator.integ(&f_ln_sqrt_inv,0,1,1e-6,1e-6));

    cout << endl << "--------------------------------------------------------" << endl;
    cout << "Using absolute and relative tolerance equal to 10⁻¹⁸" << endl;
    cout << "Integrating 4*sqrt(1-(1-x)²) from x=0 to x=1 - should be PI:" << endl;

    double Q = integrator.integ(&f_pi,0,1,1e-18,1e-18);
    printf("Result:      %.45g\n",Q);
    printf("True value:  3.14159265358979323846264338327950288419716939\n");
    cout << "Number of calls to integrand: " << calls << endl << endl;
    calls = 0;
    return 0;
}

void print_result(double Q){
    cout << "Result: " << Q << endl;
    cout << "Number of calls to integrand: " << calls << endl << endl;
    calls = 0;
}

double f_sqrt(double x){
    calls++;
    return sqrt(x);
}

double f_sqrt_inv(double x){
    calls++;
    return 1.0/sqrt(x);
}

double f_ln_sqrt_inv(double x){
    calls++;
    return log(x)/sqrt(x);
}

double f_pi(double x){
    calls++;
    return 4.0*sqrt(1-pow(1-x,2));
}


