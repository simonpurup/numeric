#include <iostream>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include "integrator.h"

using namespace std;

int calls;
double f_pi(double x);
double f_0(double x);
double f_1(double x);

void print_result(double Q);
double f(double x, void * params);
double f2(double x, void * params);

int main(){


    cout << "Using absolute and relative tolerance equal to 10⁻¹²"<< endl << endl;
    cout << "Integrating 1/((x+1)sqrt(x)) from x=0 to x=inf - should be PI:" << endl;
    Integrator integrator(&f_pi);
    double Q = integrator.integ(0,INFINITY,1e-12,1e-12);
    printf("Result:      %.45g\n",Q);
    printf("True value:  3.14159265358979323846264338327950288419716939\n");
    cout << "Number of calls to integrand: " << calls << endl << endl;
    calls = 0;

    cout << "Integrating 6x³/(x⁴+1)² from x=-inf to x=inf - should be 0:" << endl;
    print_result(integrator.integ(&f_0,-INFINITY,INFINITY,1e-12,1e-12));

    cout << "Integrating exp(1/2)/x² from x=-inf to x=0 - should be 1:" << endl;
    print_result(integrator.integ(&f_1,-INFINITY,0,1e-12,1e-12));

    cout << "#############################################################" << endl;
    double result, error;
    gsl_function F;
    F.function = &f;
    gsl_integration_workspace * w = gsl_integration_workspace_alloc (1e6);
    cout << "- With gsl_integration_qagi -" << endl;
    cout << "Integrating 1/((x+1)sqrt(x)) from x=0 to x=inf with gsl qagi function" << endl;

    calls=0;
    gsl_integration_qagi (&F, 1e-12, 1e-12, 1e6, w, &result, &error);
    printf("Result:      %.45g\n",result);
    cout << "Number of calls to integrand: " << calls << endl << endl;

    cout << "Integrating exp(1/2)/x² from x=0 to x=inf with gsl qagi function" << endl;
    F.function = &f2;
    calls=0;
    gsl_integration_qagil (&F, 0.0, 1e-12, 1e-12, 1e6, w, &result, &error);
    printf("Result:      %.45g\n",result);
    cout << "Number of calls to integrand: " << calls << endl << endl;

    gsl_integration_workspace_free (w);
}

void print_result(double Q){
    cout << "Result: " << Q << endl;
    cout << "Number of calls to integrand: " << calls << endl << endl;
    calls = 0;
}

double f_pi(double x){
    calls++;
    return 1.0/((x+1)*sqrt(x));
}

double f(double x, void * params){
    calls++;
    return 6.0*pow(x,3)/pow(pow(x,4)+1,2);
}

double f2(double x, void * params){
    calls++;
    return exp(1/x)/(x*x);
}

double f_0(double x){
    calls++;
    return 6.0*pow(x,3)/pow(pow(x,4)+1,2);
}

double f_1(double x){
    calls++;
    return exp(1/x)/(x*x);
}

