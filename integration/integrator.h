typedef double (*funct)(double);

#ifndef NUMERIC_INTEGRATOR_H
class Integrator{
public:
    Integrator(funct f);
    double integ(double a, double b, double acc, double eps);
    double integ(funct f,double a, double b, double acc, double eps);
    void setFunction(funct f);
private:
    funct f_init;
    double a_init, b_init;
    double rec_adapt_integ(double (Integrator::*f)(double), double a, double b, double acc, double eps, double f2, double f3, int nrec);
    double f_inf_inf(double t);
    double f_a_inf(double t);
    double f_inf_a(double t);
    double f_a_b(double t);
};
#define NUMERIC_INTEGRATOR_H

#endif //NUMERIC_INTEGRATOR_H
