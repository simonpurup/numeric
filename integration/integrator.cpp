#include <math.h>
#include <stdlib.h>
#include <iostream>
#include "integrator.h"

using namespace std;
typedef double (*funct)(double);
typedef double (Integrator::*ifunct)(double); //pointer to member function.

//The implementation of the Integrator class. I have chosen to use a class, as this allows for storing initial
//parameters to allow for variable transformation in the case of infinite limits. In hind sight, this could have
//been done easier, particularly since pointer to member functions and pointer to non-member functions are
//incompatible.
//However the implementation is done, and it works, so I will keep it.
Integrator::Integrator(funct f){
f_init = f;
}

double Integrator::integ(double a, double b, double acc, double eps){
    a_init = a; b_init = b;
    ifunct f = &Integrator::f_a_b;
    if(isinf(a)){ //This here to deal with infinite limits.
        if(isinf(b)){
            a = -1; b = 1;
            f = &Integrator::f_inf_inf;
        } else{
            a = -1; b = 0;
            f = &Integrator::f_inf_a;
        }
    } else if(isinf(b)){
        a = 0; b = 1;
        f = &Integrator::f_a_inf;
    }
    double f2 = (this->*f)(a+2.0*(b-a)/6), f3 = (this->*f)(a+4*(b-a)/6); //Nodes are chosen from (48) and (51) in notes
    return rec_adapt_integ(f,a,b,acc,eps,f2,f3,0);
}

//Overlaoded function, that also implicitly changes the function to be integrated.
double Integrator::integ(funct f,double a, double b, double acc, double eps){
    f_init = f;
    return integ(a,b,acc,eps);
}

void Integrator::setFunction(funct f){
    f_init = f;
}

double Integrator::rec_adapt_integ(ifunct f, double a, double b, double acc, double eps, double f2, double f3, int nrec){
    if(nrec > 1e6){ //Ensure that function terminates, even if no convergence.
        cout << "Maximum number of recursions reached (" << nrec << ")" << endl;
        return nan("");
    }
    double f1=(this->*f)(a+(b-a)/6), f4 = (this->*f)(a+5.0*(b-a)/6);
    double Q = (b-a)*(2*f1+f2+f3+2*f4)/6; //trapezium rule
    double q = (b-a)*(f1+f2+f3+f4)/4; //rectangle rule

    double tol = acc+eps*fabs(Q), error = fabs(Q-q);
    if(error < tol)
        return Q;
    else{ //If the error is greater than the tolerance, split the interval into two and solve recursively
        double Q1 = rec_adapt_integ(f,a,(a+b)/2,acc/sqrt(2.0),eps,f1,f2,nrec+1);
        double Q2 = rec_adapt_integ(f,(a+b)/2,b,acc/sqrt(2.0),eps,f3,f4,nrec+1);
        return Q1+Q2;
    }
}

double Integrator::f_inf_inf(double t){
    double c = 1.0/(1-t*t);
    return f_init(c*t)*(1+t*t)/(c*c);
}

double Integrator::f_a_inf(double t){
    double c = 1.0/(1-t);
    return f_init(a_init+t*c)*c*c;
}

double Integrator::f_inf_a(double t){
    double c = 1.0/(1+t);
    return f_init(b_init+t*c)*c*c;
}

//This "wrapper" is needed as member function pointers cannot be converted to non-member function pointers.
//It is a bit silly, but this was the only way to circumvent this that I could think of, without writing two
//rec_adapt_integ functions (one for each type of pointer). This could be implemented instead, if this solution
//decreases performance.
double Integrator::f_a_b(double t){
    return f_init(t);
}

