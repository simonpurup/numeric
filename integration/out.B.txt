Using absolute and relative tolerance equal to 10⁻¹²

Integrating 1/((x+1)sqrt(x)) from x=0 to x=inf - should be PI:
Result:      3.14159264457610110099494704627431929111480713
True value:  3.14159265358979323846264338327950288419716939
Number of calls to integrand: 33661344

Integrating 6x³/(x⁴+1)² from x=-inf to x=inf - should be 0:
Result: -1.11022e-16
Number of calls to integrand: 4

Integrating exp(1/2)/x² from x=-inf to x=0 - should be 1:
Result: 1
Number of calls to integrand: 117292

#############################################################
- With gsl_integration_qagi -
Integrating 1/((x+1)sqrt(x)) from x=0 to x=inf with gsl qagi function
Result:      0
Number of calls to integrand: 30

Integrating exp(1/2)/x² from x=0 to x=inf with gsl qagi function
Result:      1
Number of calls to integrand: 195

