Important notes:
Source code for procedures that is used in multiple exercises can be found in the tools folder, this includes:
    Gramschmidt QR
    Newton root finding and minimization

This is done to avoid code duplication.
