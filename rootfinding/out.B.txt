Root finding on gradients of the Rosenbrock's valley function:
   d/dx(f) = 2*(200*x^3-200*x*y+x-1)
   d/dy(f) = 200*(y-x^2)
==============================================================
Starting point is: (6,10)
Initial f(x,y) = [62410,-5200]

With numerical Jacobian matrix:
--------------------------------------------------------------
Solution is: (1.00003,1.00005)
With f(x,y) = [0.000349321,-0.000147104]
Total number of function calls: 7381

With analytical Jacobian matrix:
--------------------------------------------------------------
Solution is: (1.00003,1.00005)
With f(x,y) = [0.000349321,-0.000147104]
Total number of function calls: 5863

