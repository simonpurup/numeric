#include <iostream>
#include <stdio.h>
#include <math.h>
#include <armadillo>
#include "../tools/newton/newton.h"

using namespace std;
using namespace arma;

vec Axy_func(vec p);
vec rosenbrock(vec p);
vec himmelbau(vec p);
vec three_hump_camel_gradient(vec p);

int main(){
    vec x(2);
    x(0) = 6; //Start points chosen randomly.
    x(1) = 10;
    cout << "Root finding on system consisting of:" << endl;
    cout << "   f_1(x,y) = 10000xy-1" << endl;
    cout << "   f_2(x,y) = exp(-x)+exp(-y)-(1+1/10000)" << endl;
    cout << "--------------------------------------------------------------" << endl;
    vec fx = Axy_func(x);
    printf("Starting point is: (%g,%g)\n",x(0),x(1));
    printf("Initial f(x,y) = [%g,%g]\n\n",fx(0),fx(1));
    newton_root(&Axy_func, x, 1e-6, 1e-6);
    fx = Axy_func(x);
    printf("Solution is: (%g,%g)\n",x(0),x(1));
    printf("With f(x,y) = [%g,%g]\n",fx(0),fx(1));

    cout << endl << endl << endl;
    x(0) = 6; //Start points chosen randomly.
    x(1) = 10;
    cout << "Root finding on gradients of the Rosenbrock's valley function:" << endl;
    cout << "   d/dx(f) = 2(200x³-200xy+x-1)" << endl;
    cout << "   d/dy(f) = 200(y-x²)" << endl;
    cout << "--------------------------------------------------------------" << endl;
    fx = rosenbrock(x);
    printf("Starting point is: (%g,%g)\n",x(0),x(1));
    printf("Initial f(x,y) = [%g,%g]\n\n",fx(0),fx(1));
    newton_root(&rosenbrock, x, 1e-6, 1e-6);
    fx = rosenbrock(x);
    printf("Solution is: (%g,%g)\n",x(0),x(1));
    printf("With f(x,y) = [%g,%g]\n",fx(0),fx(1));

    cout << endl << endl << endl;
    x(0) = 6; //Start points chosen randomly.
    x(1) = 10;
    cout << "Root finding on gradients of the Himmelbau's function:" << endl;
    cout << "   d/dx(f) = 2(x(2x²+2y-21)+x²-7)" << endl;
    cout << "   d/dy(f) = 2(x²+2y(x+y²-7)+y-11)" << endl;
    cout << "--------------------------------------------------------------" << endl;
    fx = himmelbau(x);
    printf("Starting point is: (%g,%g)\n",x(0),x(1));
    printf("Initial f(x,y) = [%g,%g]\n\n",fx(0),fx(1));
    newton_root(&himmelbau, x, 1e-6, 1e-6);
    fx = himmelbau(x);
    printf("Solution is: (%g,%g)\n",x(0),x(1));
    printf("With f(x,y) = [%g,%g]\n",fx(0),fx(1));

    cout << endl << endl << endl;
    x(0) = 6; //Start points chosen randomly.
    x(1) = 10;
    cout << "Root finding on gradients of the Three hump camel function:" << endl;
    cout << "   d/dx(f) = x⁵-4.2x³+4x+y" << endl;
    cout << "   d/dy(f) = x+2y" << endl;
    cout << "--------------------------------------------------------------" << endl;
    fx = three_hump_camel_gradient(x);
    printf("Starting point is: (%g,%g)\n",x(0),x(1));
    printf("Initial f(x,y) = [%g,%g]\n\n",fx(0),fx(1));
    newton_root(&three_hump_camel_gradient, x, 1e-6, 1e-6);
    fx = three_hump_camel_gradient(x);
    printf("Solution is: (%g,%g)\n",x(0),x(1));
    printf("With f(x,y) = [%g,%g]\n",fx(0),fx(1));
    return 0;
}

vec Axy_func(vec p){
    vec fx(2);
    double x = p(0), y = p(1), A = 10000;
    fx(0) = A*x*y-1;
    fx(1) = exp(-x)+exp(-y)-1-1/A;
    return fx;
}

vec rosenbrock(vec p){
    vec fx(2);
    double x = p(0), y = p(1);
    fx(0) = 2.0*(200*x*x*x-200*x*y+x-1); //x derivative
    fx(1) = 200.0*(y-x*x); //y derivative
    return fx;
}

vec himmelbau(vec p){
    vec fx(2);
    double x = p(0), y = p(1);
    fx(0) = 2.0*(x*(2.0*x*x+2.0*y-21)+y*y-7); //x derivative
    fx(1) = 2.0*(x*x+2.0*y*(x+y*y-7)+y-11); //y derivative
    return fx;
}

vec three_hump_camel_gradient(vec p){
    vec grad(2);
    double x = p(0), y = p(1);
    grad(0) = pow(x,5)-4.2*pow(x,3)+4.0*x+y;
    grad(1) = x+2.0*y;
    return grad;
}