#include <iostream>
#include <stdio.h>
#include <math.h>
#include <armadillo>
#include "../tools/newton/newton.h"

using namespace std;
using namespace arma;

int ncalls;
vec rosenbrock(vec p);
mat rosenbrockJacobian(vec P);

int main() {
    ncalls = 0;
    vec x(2);
    x(0) = 6; //Start points chosen randomly.
    x(1) = 10;
    cout << "Root finding on gradients of the Rosenbrock's valley function:" << endl;
    cout << "   d/dx(f) = 2*(200*x^3-200*x*y+x-1)" << endl;
    cout << "   d/dy(f) = 200*(y-x^2)" << endl;
    cout << "==============================================================" << endl;
    vec fx = rosenbrock(x);
    printf("Starting point is: (%g,%g)\n", x(0), x(1));
    printf("Initial f(x,y) = [%g,%g]\n\n", fx(0), fx(1));

    newton_root(&rosenbrock, x, 1e-3, 1e-6);
    fx = rosenbrock(x);
    cout << "With numerical Jacobian matrix:" << endl;
    cout << "--------------------------------------------------------------" << endl;
    printf("Solution is: (%g,%g)\n", x(0), x(1));
    printf("With f(x,y) = [%g,%g]\n", fx(0), fx(1));
    cout << "Total number of function calls: " << ncalls-2<<endl<<endl;

    x(0) = 6; x(1) = 10;
    ncalls = 0;
    newton_root(&rosenbrock, &rosenbrockJacobian, x, 1e-3);
    cout << "With analytical Jacobian matrix:" << endl;
    cout << "--------------------------------------------------------------" << endl;
    printf("Solution is: (%g,%g)\n", x(0), x(1));
    printf("With f(x,y) = [%g,%g]\n", fx(0), fx(1));
    cout << "Total number of function calls: " << ncalls<<endl<<endl;
}

vec rosenbrock(vec p){
    ncalls++;
    vec fx(2);
    double x = p(0), y = p(1);
    fx(0) = 2.0*(200*x*x*x-200*x*y+x-1); //x derivative
    fx(1) = 200.0*(y-x*x); //y derivative
    return fx;
}

mat rosenbrockJacobian(vec p){
    mat J(2,2);
    double x = p(0), y = p(1);
    J(0,0) = 1200.0*x*x-400.0*y+2.0;
    J(0,1) = -400.0*x;
    J(1,0) = -400.0*x;
    J(1,1) = 200.0;
    return J;
}
